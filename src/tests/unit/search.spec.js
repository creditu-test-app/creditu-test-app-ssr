import { renderToString } from '@vue/server-test-utils'
import Search from '@/modules/search/views/Search.vue';
import app from '@/entry-server.js';


describe('Search component', () =>{

    test('renders correctly on server ', () => {
        const str = renderToString(Search)
        expect(str).toMatchSnapshot()
    })

    test('/top returns 200', async () => {
        return await request(app)
          .get('/top')
          .expect(200)
      }).rejects.toThrowError();

});

describe('InstantSearch - Page navigation', () => {
 
    it('must have the expected results for page 1', async () => {
      const hitsTitles = await browser.getHitsTitles();
  
      expect(hitsTitles).toEqual([
        'fgzywj jazfl',
        'dquyja crdgj',
        'gmjmb gjrkwh',
        'uhiscx ñsdbs',
        'ñzpj zcvkbdqm',
        'xbcmy qjlmfn',
        'geblup hhotp',
        'oghawh brvsb',
        'imvxt tpjhcg',
        'uuhrb jvwtwq',
        'zymart xqisc',
        'lierzr ewacf',
        'jrdq edwwvtjj',
        'xxñhka outwj',
        'qkdy kkhvhfgq',
        'hmmh pwdfufño',
        'lamoi egcnxb',
        'exqñvj ttunh',
        'nqvx cslkiñcd',
        'nwoj sxmtrzbe'
      ]);
    });
  
    it('must reset the page to 1', async () => {
      const page = await browser.getCurrentPage();
  
      expect(page).toEqual(1);
    });
  
  });

  describe('InstantSearch - Search on specific name and query filtering', () => {

    it('navigates to the search url', async () => {
      await browser.url('/search');
    });
    
    it('fills search input with "fgzywj jazfl"', async () => {
      await browser.setSearchBoxValue('fgzywj jazfl');
    });
  
    it('waits for the results list to be updated (wait for the "fgzywj jazfl" word to be highlighted)', async () => {
      await browser.waitForElement('mark=fgzywj jazfl');
    });
  
    it('must have the expected results', async () => {
      const hitsTitles = await browser.getHitsTitles();
  
      expect(hitsTitles).toEqual([
        'aypwtañ fgzywjjazfl',
        'breizhf fgzywjjazfl',
        'dsfsd fgzywjjazfl',
        'rrgbñvoid fgzywjjazfl',
        'fvtfdñm fgzywjjazfl',
        'tvgbggj fgzywjjazfl',
        'giw knqhzñd fgzywjjazfl',
        'akh flnzgbj fgzywjjazfl',
        'ikd qmflpsq fgzywjjazfl',
        'kox uolgirm fgzywjjazfl',
        
        
      ]);
    });
  });

